# DiagramRepositoryAPI

This project is C# .NET Framework 4.7

## Requirements

* Visual Studio 2019
* .NET Framework 4.7

## Build

1. Either clone project from master branch, or pull to ensure all changes are up to date.
2. Run Build -> `Rebuild Solution` from Visual studio 2019 to build the project.