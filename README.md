# Diagram Repository

Provide a centralized location to allow users to upload diagrams for applications, view diagrams, add notes for diagrams, and download diagrams.